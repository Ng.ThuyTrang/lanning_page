import { Welcome } from "./welcome/Welcome";
import { Champion } from "./champion/Champion";
import { ChampionCard } from "./champion/ChampionCard";
import { ChampionDetail } from "./champion/ChampionDetail";
import { Trailer } from "./trailer/Trailer";
import {Credit}  from "./credit/Credit";

export { Welcome, Champion, ChampionCard, ChampionDetail, Trailer, Credit };
