import { Section, SectionContent } from "../section/Section"

export const HomeSection = (props) => {
    return (
        <Section
            className={props.className}
        >
            <SectionContent
                className={props.contentClassName}
                bgImage={props.bgImage}
            >
                {props.children}
            </SectionContent>
        </Section>
    )
}

